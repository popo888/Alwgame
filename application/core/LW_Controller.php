<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Admin extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Shanghai');
        $this->load->library('session');
        $this->load->library('golbal');
        $this->load->helper('url');
    }
    
    public function check_login() {
        if(!isset($_SESSION['username']) && !isset($_SESSION['admin_id'])){
            redirect(base_url('/user/index'));
        }
    }
}
