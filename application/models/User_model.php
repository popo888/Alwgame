<?php
class  User_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Shanghai');

    }

    //登陆开始
    function login_action($username,$pw){
        $ret=$this->db->query("SELECT * from lw_admin where username='$username'");
        if($ret->num_rows()>0){
            $userinfo=$ret->row_array();
            if($userinfo['password']!=$pw){
                return '1';  //密码错误
            }else{  //成功，把手机号和管理员ID 放入session
                $session_arr = array(
                    'username'=>$userinfo['username'],
                    'admin_id'=>$userinfo['id']
                );
                $this->session->set_userdata($session_arr);
                $time=time();
                $this->db->query("update lw_admin set last_time=$time");
                return '2'; //登陆成功 修改登陆最后次登陆时间
            }
        }else{
            return '3'; //暂无管理员
        }
    }
    
    function get_user_list(){
        $list = array();
        $sql = "select id,username,mobile,member_class,create_time,remark,status from ".$this->db->dbprefix('user');
        $ret = $this->db->query($sql);
        foreach ($ret->result_array() as $va) {
            $arr['id'] = $va['id'];
            $arr['username'] = $va['username'];
            $arr['mobile'] = $va['mobile'];
            if($va['member_class'] == 0){
                $arr['member_class'] = "普通会员";
            }elseif($va['member_class'] == 1){
                $arr['member_class'] = "白银会员";
            }else{
                $arr['member_class'] = "黄金会员";
            }
            $arr['create_time'] = $va['create_time'];
            $arr['remark'] = $va['remark'];
            if($va['status'] == 1){
                $arr['status'] = "启用";
            }else{
                $arr['status'] = "锁定";
            }
            $list[] = $arr;
        }
        return $list;
    }
}
?>