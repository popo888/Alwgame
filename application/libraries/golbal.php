<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class golbal {
    public function __construct()
    {
        date_default_timezone_set('Asia/Shanghai');
        $this->CI =& get_instance();
        $this->CI->load->library('session');
    }

     //验证码
    public function load_img(){
        if($path=@$_SESSION['filename']){
            if (file_exists('./captcha/'.$path)) {
                unlink('./captcha/'.$path);
            }
        }
        $path=realpath(dirname(__FILE__) . '/fonts/');
        $this->CI->load->helper('captcha');
        $vals = array(
            'word'      => rand(1000, 10000),
            'img_path'  => './captcha/',
            //'img_url'   => 'http://jl.haodbao.com/captcha/',
            //'img_url'   => 'http://app.51edb.com/captcha/',
            'img_url'   =>'./captcha/',
            'font_path' => $path.'/22.ttf',
            'img_width' => '100',
            'img_height'    => 30,
            'expiration'    => 7200,
            'word_length'   => 8,
            'font_size' => 20,
            'img_id'    => 'Imageid',
            'pool'      => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'    => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
            )
        );

        $cap = create_captcha($vals);
        $newdata = array(
            'filename'  => $cap['filename'],
            'word'     => $cap['word'],
            'time' => md5($cap['time'])
        );

        $this->CI->session->set_userdata($newdata);
        $cap['time']=md5($cap['time']);
        $arr = array("filename" => $cap['filename'], "time" => $cap['time']);
        return $arr;
    }
}
?>