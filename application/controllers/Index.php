<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Admin {
    
    public function __construct() {
        parent::__construct();
    }

    function main(){
       $this->check_login();
       $this->load->view('index');
    }
}
