<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        
    }
    //登陆静态页面+验证码
    public function index(){
        $arr = $this->golbal->load_img();
        $this->load->view('login',$arr);
    }

    //验证码切换
    public function change_img(){
        $arr = $this->golbal->load_img();
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);
    }

    //登陆开始
    public function login_action(){
        $username=$this->input->post('username');
        $pw=$this->input->post('pass');
        $code=$this->input->post('code');
        //验证码
        if(trim($code)!=$_SESSION['word']){
            $arr = array(
                'code' => '0',
                'msg' => '验证码错误'
            );
            echo json_encode($arr,JSON_UNESCAPED_UNICODE);
            exit();
        }

        $ret=$this->User_model->login_action($username,md5($pw));
        switch ($ret) {
            case '1':
                $arr = array(
                    'code' => '1',
                    'msg' => '密码错误'
                );
                break;
            case '2':
                $arr = array(
                    'code' => '2',
                    'msg' => '登陆成功'
                );
                break;
            case '3':
                $arr = array(
                    'code' => '3',
                    'msg' => '管理员不存在'
                );
                break;

        }
        echo json_encode($arr,JSON_UNESCAPED_UNICODE);

    }
    
    public function userlist(){
        $this->load->view('user_list');
    }
    
    public function getlist(){
        $userlist = $this->User_model->get_user_list();
        echo json_encode($userlist);
    }
    
    public function log_out(){
        unset(
            $_SESSION['username'],
            $_SESSION['admin_id']
        );
        redirect('/user/index');
    }
}
