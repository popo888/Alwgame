<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <title> - 登录</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="/public/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="/public/css/animate.css" rel="stylesheet">
    <link href="/public/css/style.css" rel="stylesheet">
    <link href="/public/css/login.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>
        if (window.top !== window.self) {
            window.top.location = window.location;
        }
    </script>

</head>

<body class="signin">
    <div class="signinpanel">
        <div class="row">
            <div class="col-sm-12">
                <form method="post" action="index.html">
                    <h2 style="text-align: center">登录</h2>
                    <input id="username" type="text" class="form-control uname" placeholder="用户名" />
                    <input id="pass" type="password" class="form-control pword m-b" placeholder="密码" />
                    <div>
                        <input id="code" type="text" style="width: 50%;float: left;margin-top: 0;margin-right: 15px" class="form-control" placeholder="验证码" />
                        <img id="img" onclick="change_img()" src="/captcha/<?php echo $filename  ?>">
                    </div>
                    <div class="btn btn-success btn-block" onclick="go();">登录</div>
                </form>
            </div>
        </div>
        <div class="signup-footer">
            <div class="pull-left">
                &copy; cdk
            </div>
        </div>
    </div>
</body>
<script src="/public/js/jquery.min.js?v=2.1.4"></script>
<script type="text/javascript">

    function change_img(){
        $.ajax({
            type:'post',
            url: '/user/change_img',
            data:{},
            success:function(result){
                var result =JSON.parse(result);
                var html='';
                html+='<img id="img" onclick="change_img();" src="/captcha/'+result.filename+'" class="code-img">';
                $("#img").replaceWith(html);
                 //alert(result.msg)
            }
        });
    }
 function go(){
    var username = $('#username').val();
    var pass = $.trim($('#pass').val());
    var code = $.trim($('#code').val());
    console.log(!username);
     if(!username || !pass || !code){
        alert("请填写完整")
        return false;
     }
    $.ajax({
        type:'post',
        url: '/user/login_action',
        data:{
            username: username,
            pass: pass,
            code:code
        },
        success:function(result){
            var result =JSON.parse(result);
            alert(result.msg);
            if(result.code=='2'){
                window.location.href="/index/main";
            }
        }
    });

 }
</script>
</html>
