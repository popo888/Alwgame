<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - Bootstrap Table</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico"> <link href="/public/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/public/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="/public/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="/public/css/animate.css" rel="stylesheet">
    <link href="/public/css/style.css?v=4.1.0" rel="stylesheet">
    

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <!-- Panel Other -->
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>用户列表</h5>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <div class="col-sm-12">
                        <!-- Example Events -->
<!--                        <div class="example-wrap">
                            <h4 class="example-title">事件</h4>
                            <div class="example">
                                <div class="alert alert-success" id="examplebtTableEventsResult" role="alert">
                                    事件结果
                                </div>
                                <div class="btn-group hidden-xs" id="exampleTableEventsToolbar" role="group">
                                    <button type="button" class="btn btn-outline btn-default">
                                        <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                                    </button>
                                    <button type="button" class="btn btn-outline btn-default">
                                        <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                                    </button>
                                    <button type="button" class="btn btn-outline btn-default">
                                        <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                                    </button>
                                </div>-->
                                <table 
                                    data-toggle="table" 
                                    data-url="/user/getlist"
                                    data-striped="true"
                                    data-query-params="queryParams" 
                                    data-mobile-responsive="true" 
                                    data-height="400" 
                                    data-pagination="true" 
                                    data-icon-size="outline" 
                                    data-search="true">
                                    <thead>
                                        <tr>
                                            <th data-field="state" data-checkbox="true"></th>
                                            <th data-field="id">ID</th>
                                            <th data-field="username">用户名</th>
                                            <th data-field="mobile">手机</th>
                                            <th data-field="member_class">会员等级</th>
                                            <th data-field="create_time">注册时间</th>
                                            <th data-field="remark">备注</th>
                                            <th data-field="status">是否启用</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        <!--</div>-->
                        <!-- End Example Events -->
                    <!--</div>-->
                </div>
            </div>
        </div>
        <!-- End Panel Other -->
    </div>

    <!-- 全局js -->
    <script src="/public/js/jquery.min.js?v=2.1.4"></script>
    <script src="/public/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- 自定义js -->
    <script src="/public/js/content.js?v=1.0.0"></script>


    <!-- Bootstrap table -->
    <script src="/public/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="/public/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
    <script src="/public/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

    <!-- Peity -->
    <script src="/public/js/demo/bootstrap-table-demo.js"></script>

    
    

</body>

</html>
